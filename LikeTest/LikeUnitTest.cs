﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LikeTest
{
    [TestClass]
    public class LikeUnitTest
    {

        [TestMethod]
        public void TestLike()
        {
            if (!Alivate.SQLMethods.EvaluateIsLike("abc% abd 123 %", "%b%"))
                throw new Exception("Failed Test!");

            if (!Alivate.SQLMethods.EvaluateIsLike("abc% abd 123 %", "%bd%"))
                throw new Exception("Failed Test!");

            if (!Alivate.SQLMethods.EvaluateIsLike("abc% abd 123 %", "%!%%"))
                throw new Exception("Failed Test!");

            if (!Alivate.SQLMethods.EvaluateIsLike("abc% abd 123 %", "%!%%!%%"))
                throw new Exception("Failed Test!");

            if (!Alivate.SQLMethods.EvaluateIsLike("abc% abd 123 %", "%!%%!%"))
                throw new Exception("Failed Test!");

            if (!Alivate.SQLMethods.EvaluateIsLike("abc% abd 123 %", "abc!%%!%"))
                throw new Exception("Failed Test!");

            if (!Alivate.SQLMethods.EvaluateIsLike("abc% abd 123 %", "ab_%"))
                throw new Exception("Failed Test!");

            if (!Alivate.SQLMethods.EvaluateIsLike("abc% abd 123 %", "ab_%"))
                throw new Exception("Failed Test!");

            if (!Alivate.SQLMethods.EvaluateIsLike("abc!", "%!!"))
                throw new Exception("Failed Test!");

            if (Alivate.SQLMethods.EvaluateIsLike("abc% abd 123 %", "!%!%_"))
                throw new Exception("Failed Test!");

            if (Alivate.SQLMethods.EvaluateIsLike("abc% abd 123 %", "123 !%%_"))
                throw new Exception("Failed Test!");

            if (Alivate.SQLMethods.EvaluateIsLike("abc% abd 123 %", "%bc%bc%"))
                throw new Exception("Failed Test!");

            if (Alivate.SQLMethods.EvaluateIsLike("abc% abd 123 %", "%!%!%%"))
                throw new Exception("Failed Test!");

            if (Alivate.SQLMethods.EvaluateIsLike("abc% abd 123 %", "%!%!%"))
                throw new Exception("Failed Test!");

            if (Alivate.SQLMethods.EvaluateIsLike("abc% abd 123 %", "abc!%!%"))
                throw new Exception("Failed Test!");

            if (Alivate.SQLMethods.EvaluateIsLike("abc% abd 123 %", "abc%_"))
                throw new Exception("Failed Test!");

            if (!Alivate.SQLMethods.EvaluateIsLike("abc% abd 123 %", "abc%%"))
                throw new Exception("Failed Test!");

            if (!Alivate.SQLMethods.EvaluateIsLike("abc% abd 123 %", "abc_ a%"))
                throw new Exception("Failed Test!");

            if (Alivate.SQLMethods.EvaluateIsLike("abc% abd 123", "abc%123_"))
                throw new Exception("Failed Test!");

            if (Alivate.SQLMethods.EvaluateIsLike("abc% abd 123", "abc%abd "))
                throw new Exception("Failed Test!");

            try
            {
                Alivate.SQLMethods.EvaluateIsLike("abc% abd 123 %", "abc!%!");
                throw new Exception("Failed Test!");
            }
            catch (Exception ex)
            {
                if (ex.Message != "Escape character found at end of string - can't use that")
                    Assert.Fail("Failed Test!");
            }

            try
            {
                Alivate.SQLMethods.EvaluateIsLike("abc% abd 123 %", "abc%!");
                throw new Exception("Failed Test!");
            }
            catch (Exception ex)
            {
                if (ex.Message != "Escape character found at end of string - can't use that")
                    Assert.Fail("Failed Test!");
            }

            try
            {
                Alivate.SQLMethods.EvaluateIsLike("ab!c", "ab!c");
                throw new Exception("Failed Test!");
            }
            catch (Exception ex)
            {
                if (!ex.Message.StartsWith("Invalid escape sequence - "))
                    Assert.Fail("Escape character without following character");
            }
            
        }
    }
}
